+++
title = "Privacy"
description = "We take privacy and data seriously and to that front we don't save or track any of user data."

+++

## Data and Processing
The app(GitNex) only store basic information related to your account on the device. These data remain strictly confidential and are only available to the application. Removing the application will also remove the stored data.

No passwords are stored locally on the app. It is only used to get the access token from Gitea instance. This token can be revoked from settings on Gitea web ui.  
*\*Except for using basic http authentication where it is required to save the password and you should be aware of it. GitNex prefer to obtain token from the instance but not in case of http authentication. Password is saved in the app secure storage and is not accessible to any other apps, upon clearing app data or removing the app, all the data will be erased. Read [here](https://gitea.com/gitnex/GitNex/wiki/Troubleshoot-Guide) for using this feature.*

All connections are relayed through SSL encryption by default to an instance unless user choose to select **HTTP** in the login screen. In that case user should be aware of the consequences and GitNex as an app will not be held accountable for any MITM for example.

## Required Permissions
- INTERNET ACCESS [android.permission.INTERNET]
- NETWORK STATE [android.permission.ACCESS_NETWORK_STATE]

## Tracking
GitNex does not use any tracking tools and does not run or facilitate advertising.
