+++
description = "GitNex is a free, open-source Android client for Git repository management tool Gitea. Gitea is a community managed fork of Gogs, lightweight code hosting solution written in Go."
title = "GitNex"
draft = false

+++
