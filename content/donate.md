+++
title = "Donate"
description = "Every dollar counts and that's where all of you good people shows your love for GitNex."

+++

## Become a Patreon
[<img alt="Become a Patroen" src="/img/become_a_patron_button.png" id="patreon_button"/>](https://www.patreon.com/mmarif)

## Donate via Liberapay
[<img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"/>](https://liberapay.com/mmarif/donate)