+++
title = "Sponsors"
description = "Thank you for helping the project."

+++

All the sponsor names are listed in the app About -> Sponsors section.

Thanks to all the sponsors for the generous donations which keep the development going and help cover the infrastructure and other costs.