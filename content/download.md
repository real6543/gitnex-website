+++
title = "Download"
description = "Download using your preferred method."

+++

## Download or install from F-droid store  
[<img alt='Get it on F-droid' src='/img/get-it-on-fdroid.png' height="80" width="200"/>](https://f-droid.org/en/packages/org.mian.gitnex)

## Download or install from Goole Play store
[<img alt='Get it on Google Play' src='/img/google_play.png' height="80" width="200"/>](https://play.google.com/store/apps/details?id=org.mian.gitnex)

## Download APK  
[<img alt='Get APK' src="/img/download-apk.png">](https://gitea.com/gitnex/GitNex/releases)
